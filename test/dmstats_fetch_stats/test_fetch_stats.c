/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>

#include "mod_dmstats.h"

#include "dummy_be.h"
#include "test_fetch_stats.h"

static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;
static amxd_dm_t dm;

int test_fetch_stats_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    amxd_dm_init(&dm);
    amxo_parser_init(&parser);
    root_obj = amxd_dm_get_root(&dm);

    assert_int_equal(test_register_dummy_be(), 0);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    assert_int_equal(test_load_dummy_remote("../mock/netdev.odl"), 0);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "stats_object_read", AMXO_FUNC(_stats_object_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "stats_object_list", AMXO_FUNC(_stats_object_list)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "stats_object_describe", AMXO_FUNC(_stats_object_describe)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, "./test_dm.odl", root_obj), 0);

    return 0;
}

int test_fetch_status_teardown(UNUSED void** state) {
    amxb_disconnect(bus_ctx);
    amxb_free(&bus_ctx);

    test_unregister_dummy_be();

    amxd_dm_clean(&dm);
    amxo_parser_clean(&parser);

    return 0;
}

void test_can_list_stats_parameters(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    amxc_var_t params;
    const amxc_llist_t* lparams = NULL;

    amxc_var_init(&params);

    obj = amxd_dm_findf(&dm, "Interface.1.Stats.");
    assert_non_null(obj);

    assert_int_equal(amxd_object_list_params(obj, &params, amxd_dm_access_public), 0);
    amxc_var_dump(&params, STDOUT_FILENO);

    lparams = amxc_var_constcast(amxc_llist_t, &params);
    assert_non_null(lparams);
    assert_int_equal(amxc_llist_size(lparams), 15);

    obj = amxd_dm_findf(&dm, "NotExistingInf.Stats.");
    assert_non_null(obj);

    assert_int_equal(amxd_object_list_params(obj, &params, amxd_dm_access_public), 0);
    amxc_var_dump(&params, STDOUT_FILENO);

    lparams = amxc_var_constcast(amxc_llist_t, &params);
    assert_non_null(lparams);
    assert_int_equal(amxc_llist_size(lparams), 15);

    obj = amxd_dm_findf(&dm, "NoName.Stats.");
    assert_non_null(obj);

    assert_int_equal(amxd_object_list_params(obj, &params, amxd_dm_access_public), 0);
    amxc_var_dump(&params, STDOUT_FILENO);

    lparams = amxc_var_constcast(amxc_llist_t, &params);
    assert_non_null(lparams);
    assert_int_equal(amxc_llist_size(lparams), 15);

    amxc_var_clean(&params);
}

void test_can_read_stats(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    amxc_var_t stats;

    amxc_var_init(&stats);

    obj = amxd_dm_findf(&dm, "Interface.1.Stats.");
    assert_non_null(obj);

    assert_int_equal(amxd_object_get_params(obj, &stats, amxd_dm_access_public), 0);
    amxc_var_dump(&stats, STDOUT_FILENO);

    assert_int_not_equal(GET_UINT32(&stats, "BytesReceived"), 0);
    assert_int_not_equal(GET_UINT32(&stats, "BytesSent"), 0);
    assert_int_not_equal(GET_UINT32(&stats, "ErrorsReceived"), 0);
    assert_int_not_equal(GET_UINT32(&stats, "ErrorsSent"), 0);
    assert_int_not_equal(GET_UINT32(&stats, "PacketsReceived"), 0);
    assert_int_not_equal(GET_UINT32(&stats, "PacketsSent"), 0);

    //Testing the output when the parameter is not found
    amxc_var_clean(&stats);
    amxc_var_init(&stats);
    assert_int_equal(amxd_object_get_params_filtered(obj, &stats, "Status", amxd_dm_access_public), amxd_status_parameter_not_found);
    amxc_var_clean(&stats);
    amxc_var_init(&stats);
    assert_int_not_equal(amxd_object_get_params_filtered(obj, &stats, "", amxd_dm_access_public), amxd_status_parameter_not_found);
    amxc_var_clean(&stats);
}

void test_can_read_stats_intf_does_not_exists(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    amxc_var_t stats;

    amxc_var_init(&stats);

    obj = amxd_dm_findf(&dm, "NotExistingInf.");
    assert_non_null(obj);

    assert_int_equal(amxd_object_get_params(obj, &stats, amxd_dm_access_public), 0);
    amxc_var_dump(&stats, STDOUT_FILENO);

    assert_int_equal(GET_UINT32(&stats, "BytesReceived"), 0);
    assert_int_equal(GET_UINT32(&stats, "BytesSent"), 0);
    assert_int_equal(GET_UINT32(&stats, "ErrorsReceived"), 0);
    assert_int_equal(GET_UINT32(&stats, "ErrorsSent"), 0);
    assert_int_equal(GET_UINT32(&stats, "PacketsReceived"), 0);
    assert_int_equal(GET_UINT32(&stats, "PacketsSent"), 0);

    amxc_var_clean(&stats);
}

void test_can_describe_stats_parameters(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    amxc_var_t stats;

    amxc_var_init(&stats);

    obj = amxd_dm_findf(&dm, "Interface.2.Stats.");
    assert_non_null(obj);

    assert_int_equal(amxd_object_describe_params(obj, &stats, amxd_dm_access_public), 0);
    amxc_var_dump(&stats, STDOUT_FILENO);

    assert_int_not_equal(GET_UINT32(&stats, "BytesReceived"), 0);
    assert_int_not_equal(GET_UINT32(&stats, "BytesSent"), 0);
    assert_int_not_equal(GET_UINT32(&stats, "ErrorsReceived"), 0);
    assert_int_not_equal(GET_UINT32(&stats, "ErrorsSent"), 0);
    assert_int_not_equal(GET_UINT32(&stats, "PacketsReceived"), 0);
    assert_int_not_equal(GET_UINT32(&stats, "PacketsSent"), 0);

    amxc_var_clean(&stats);
}
