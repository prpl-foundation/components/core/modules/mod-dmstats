/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_object.h>
#include <amxd/amxd_action.h>

#include <amxb/amxb.h>

#include "mod_dmstats.h"

/**
 *  Struct used to create a list of information about the stats parameters
 */
typedef struct StatsParameter {
    const char* name;     // Name of the parameter
    uint32_t type;        // Parameter type
    const char* pname;    // Name for lookup at the location
    const char* location; // currently only netdev supported, but other lookup locations could be added
} StatsParameter_t;


/**
 * Definition for the stats parameters
 */
static StatsParameter_t StatsParameters[] = {
    {"BytesSent", AMXC_VAR_ID_UINT64, "TxBytes", "netdev"},
    {"BytesReceived", AMXC_VAR_ID_UINT64, "RxBytes", "netdev"},
    {"PacketsSent", AMXC_VAR_ID_UINT64, "TxPackets", "netdev"},
    {"PacketsReceived", AMXC_VAR_ID_UINT64, "RxPackets", "netdev"},
    {"ErrorsSent", AMXC_VAR_ID_UINT32, "TxErrors", "netdev"},
    {"ErrorsReceived", AMXC_VAR_ID_UINT32, "RxErrors", "netdev"},
    {"UnicastPacketsSent", AMXC_VAR_ID_UINT64, "", ""},
    {"UnicastPacketsReceived", AMXC_VAR_ID_UINT64, "", ""},
    {"DiscardPacketsSent", AMXC_VAR_ID_UINT32, "", ""},
    {"DiscardPacketsReceived", AMXC_VAR_ID_UINT32, "", ""},
    {"MulticastPacketsSent", AMXC_VAR_ID_UINT64, "", ""},
    {"MulticastPacketsReceived", AMXC_VAR_ID_UINT64, "", ""},
    {"BroadcastPacketsSent", AMXC_VAR_ID_UINT64, "", ""},
    {"BroadcastPacketsReceived", AMXC_VAR_ID_UINT64, "", ""},
    {"UnknownProtoPacketsReceived", AMXC_VAR_ID_UINT32, "", ""},
    { NULL, 0, NULL, NULL}
};

/**
 * Get the iface name for the corresponding stats object
 */
static const char* stats_object_get_name(amxd_object_t* statsobject) {
    const char* retv = NULL;
    amxd_object_t* parent = amxd_object_get_parent(statsobject);
    const amxc_var_t* name = amxd_object_get_param_value(parent, "NetDevName");

    if(name == NULL) {
        name = amxd_object_get_param_value(parent, "Name");
    }

    retv = amxc_var_constcast(cstring_t, name);

    return retv;
}


/**
 *  Function get the netdev stats value for an object
 */
static amxc_var_t* stats_netdev_values_get(amxd_object_t* object) {
    int status = AMXB_STATUS_OK;
    amxb_bus_ctx_t* bus = amxb_be_who_has("NetDev.");
    amxc_var_t data;
    amxc_var_t* ret = NULL;
    amxc_string_t search_path;
    const char* name = stats_object_get_name(object);

    amxc_var_init(&data);
    amxc_string_init(&search_path, 0);

    when_str_empty(name, exit);

    amxc_string_setf(&search_path, "NetDev.Link.[Name == '%s'].Stats.", name);
    status = amxb_get(bus, amxc_string_get(&search_path, 0), 0, &data, 5);

    if(status == amxd_status_ok) {
        ret = amxc_var_take_index(amxc_var_get_first(&data), 0);
    }

exit:
    amxc_var_clean(&data);
    amxc_string_clean(&search_path);
    return ret;
}


/**
 * Overwrite the read function for the stats object
 */
amxd_status_t _stats_object_read(amxd_object_t* object,
                                 UNUSED amxd_param_t* param,
                                 UNUSED amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 UNUSED void* priv) {

    amxd_status_t status = amxd_status_ok;
    amxc_string_t filter;
    amxc_var_t param_descr;
    amxc_var_t* stats = NULL;
    amxp_expr_t* expr = NULL;
    amxp_expr_status_t expr_status = amxp_expr_status_ok;
    bool valid_output = false;

    amxc_var_init(&param_descr);
    amxc_string_init(&filter, 0);
    amxc_var_set_type(retval, AMXC_VAR_ID_HTABLE);

    status = amxd_action_object_read_filter(&filter, args);
    when_failed(status, exit);

    stats = stats_netdev_values_get(object);

    if(amxc_string_get(&filter, 0) != NULL) {
        when_failed(amxp_expr_new(&expr, amxc_string_get(&filter, 0)), exit);
    }

    for(int i = 0; StatsParameters[i].name != NULL; i++) {
        amxc_var_t* stat_val = NULL;

        amxc_var_set_type(&param_descr, AMXC_VAR_ID_NULL);
        amxd_param_build_description(&param_descr,
                                     StatsParameters[i].name,
                                     StatsParameters[i].type,
                                     SET_BIT(amxd_pattr_read_only) |
                                     SET_BIT(amxd_pattr_variable),
                                     NULL);

        if((expr == NULL) || amxp_expr_eval_var(expr, &param_descr, &expr_status)) {
            if((strcmp(StatsParameters[i].pname, "") != 0) && (stats != NULL)) {
                stat_val = amxc_var_get_key(stats, StatsParameters[i].pname, AMXC_VAR_FLAG_DEFAULT);
                amxc_var_take_it(stat_val);
                amxc_var_set_key(retval, StatsParameters[i].name, stat_val, AMXC_VAR_FLAG_DEFAULT);
                amxc_var_cast(stat_val, StatsParameters[i].type);
            } else {
                stat_val = amxc_var_add_new_key(retval, StatsParameters[i].name);
                amxc_var_set_type(stat_val, StatsParameters[i].type);
            }
            valid_output = true;
        }
    }

    if(!valid_output) {
        status = amxd_status_parameter_not_found;
    }

exit:
    amxp_expr_delete(&expr);
    amxc_var_clean(&param_descr);
    amxc_var_delete(&stats);
    amxc_string_clean(&filter);
    return status;
}


/**
 * Overwrite the list function for the stats object
 */
amxd_status_t _stats_object_list(amxd_object_t* object,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv) {

    amxd_status_t status = amxd_status_ok;
    amxc_var_t* params = NULL;

    status = amxd_action_object_list(object,
                                     param,
                                     reason,
                                     args,
                                     retval,
                                     priv);
    when_failed(status, exit);

    params = amxc_var_get_path(retval, "parameters", AMXC_VAR_FLAG_DEFAULT);
    for(int i = 0; StatsParameters[i].name != NULL; i++) {
        amxc_var_add(cstring_t, params, StatsParameters[i].name);

    }

exit:
    return status;
}

/**
 * Overwrite the describe function for the stats object
 */
amxd_status_t _stats_object_describe(amxd_object_t* object,
                                     amxd_param_t* param,
                                     amxd_action_t reason,
                                     const amxc_var_t* const args,
                                     amxc_var_t* const retval,
                                     void* priv) {

    amxd_status_t status = amxd_status_ok;
    amxc_var_t* stats = NULL;
    amxc_var_t* params = NULL;

    status = amxd_action_object_describe(object,
                                         param,
                                         reason,
                                         args,
                                         retval,
                                         priv);

    when_failed(status, exit);

    params = amxc_var_get_path(retval, "parameters", AMXC_VAR_FLAG_DEFAULT);
    stats = stats_netdev_values_get(object);

    for(int i = 0; StatsParameters[i].name != NULL; i++) {
        amxc_var_t* param_descr = amxc_var_add_key(amxc_htable_t,
                                                   params,
                                                   StatsParameters[i].name,
                                                   NULL);
        amxd_param_build_description(param_descr,
                                     StatsParameters[i].name,
                                     StatsParameters[i].type,
                                     SET_BIT(amxd_pattr_read_only) |
                                     SET_BIT(amxd_pattr_variable),
                                     NULL);

        if((strcmp(StatsParameters[i].pname, "") != 0) && (stats != NULL)) {
            amxc_var_t* stat_val = NULL;
            stat_val = amxc_var_get_key(stats, StatsParameters[i].pname, AMXC_VAR_FLAG_DEFAULT);
            amxc_var_take_it(stat_val);
            amxc_var_cast(stat_val, AMXC_VAR_ID_UINT32);
            amxc_var_set_key(param_descr, "Value", stat_val, AMXC_VAR_FLAG_UPDATE);
        }
    }

exit:
    amxc_var_delete(&stats);
    return status;
}
